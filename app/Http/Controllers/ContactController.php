<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contact()
    {
        return view('contact', ["title" => "Contact"], ['page' => 4]);
    }
}
