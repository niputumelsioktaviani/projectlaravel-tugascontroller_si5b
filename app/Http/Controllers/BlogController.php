<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function blog()
    {
        return view('blog', ["title" => "Blog"], ['page' => 5]);
    }

    public function blog1()
    {
        return view('blog-post1', ["title" => "Blog"], ['page' => 5]);
    }

    public function blog2()
    {
        return view('blog-post2', ["title" => "Blog"], ['page' => 5]);
    }

    public function blog3()
    {
        return view('blog-post3', ["title" => "Blog"], ['page' => 5]);
    }

    public function blog4()
    {
        return view('blog-post4', ["title" => "Blog"], ['page' => 5]);
    }

    public function blog5()
    {
        return view('blog-post5', ["title" => "Blog"], ['page' => 5]);
    }

    public function blog6()
    {
        return view('blog-post6', ["title" => "Blog"], ['page' => 5]);
    }
}
