<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('home');
Route::get('about', [App\Http\Controllers\AboutController::class, 'about'])->name('about');
Route::get('portfolio', [App\Http\Controllers\PortfolioController::class, 'portfolio'])->name('portfolio');
Route::get('contact', [App\Http\Controllers\ContactController::class, 'contact'])->name('contact');
Route::get('blog', [App\Http\Controllers\BlogController::class, 'blog'])->name('blog');
Route::get('blog-post1', [App\Http\Controllers\BlogController::class, 'blog1'])->name('blog-post');
Route::get('blog-post2', [App\Http\Controllers\BlogController::class, 'blog2'])->name('blog-post');
Route::get('blog-post3', [App\Http\Controllers\BlogController::class, 'blog3'])->name('blog-post');
Route::get('blog-post4', [App\Http\Controllers\BlogController::class, 'blog4'])->name('blog-post');
Route::get('blog-post5', [App\Http\Controllers\BlogController::class, 'blog5'])->name('blog-post');
Route::get('blog-post6', [App\Http\Controllers\BlogController::class, 'blog6'])->name('blog-post');

/*use App\Http\Controllers\BlogController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'home']);
Route::get('/about', [AboutController::class, 'about']);
Route::get('/contact', [ContactController::class, 'contact']);
Route::get('/portfolio', [PortfolioController::class, 'portfolio']);
Route::get('/blog', [BlogController::class, 'blog']);
Route::get('/blog-post1', [BlogController::class, 'blog1']);
Route::get('/blog-post2', [BlogController::class, 'blog2']);
Route::get('/blog-post3', [BlogController::class, 'blog3']);
Route::get('/blog-post4', [BlogController::class, 'blog4']);
Route::get('/blog-post5', [BlogController::class, 'blog5']);
Route::get('/blog-post6', [BlogController::class, 'blog6']); */



/*Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('home');
});

Route::view('/about', 'about');

Route::view('/portfolio', 'portfolio');

Route::view('/contact', 'contact');

Route::view('/blog', 'blog');

Route::view('/blog-post1', 'blog-post1');

Route::view('/blog-post2', 'blog-post2');

Route::view('/blog-post3', 'blog-post3');

Route::view('/blog-post4', 'blog-post4');

Route::view('/blog-post5', 'blog-post5');

Route::view('/blog-post6', 'blog-post6');*/
