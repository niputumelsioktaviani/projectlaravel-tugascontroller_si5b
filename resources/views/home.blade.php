@extends('layout')

@section('judul')
    Home
@endsection

@section('konten')
    <!-- Main Content Starts -->
    <section class="home">
        <div class="color-block d-none d-lg-block"></div>
        <div class="row home-details-container align-items-center">
            <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
            <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
                <div>
                    <img src="img/img-mobile.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none"
                        alt="my picture" />
                    <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">Hello !</h6>
                    <h1 class="text-uppercase poppins-font"><span>Saya</span> Melsi Oktaviani</h1>
                    <p class="open-sans-font">Saya adalah seorang mahasiswi di Perguruan Tinggi Universitas Pendidikan
                        Ganesha di Bali. Sekarang
                        saya sedang berada di semester 5. Saya mempunyai hobby membaca novel dan menari. Yuk, Kenali
                        saya lebih jauh lagi!</p>
                    <a href="/about" class="btn btn-about">more about me</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Main Content Ends -->
@endsection
