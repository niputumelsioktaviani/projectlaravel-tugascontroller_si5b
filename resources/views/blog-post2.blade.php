@extends('layout')

@section('judul')
    My Blog
@endsection

@section('konten')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>blog</span></h1>
        <span class="title-bg">posts</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="blog-post2">
        <div class="container">
            <div class="row">
                <!-- Article Starts -->
                <article class="col-12">
                    <!-- Meta Starts -->
                    <div class="meta open-sans-font">
                        <span><i class="fa fa-user"></i> melsi</span>
                        <span class="date"><i class="fa fa-calendar"></i> 26 September 2021</span>
                        <span><i class="fa fa-tags"></i>melsi.com</span>
                    </div>
                    <!-- Meta Ends -->
                    <!-- Article Content Starts -->
                    <h1 class="text-uppercase text-capitalize">Tips Bersosialisasi dengan Kakak Tingkat</h1>
                    <img src="{{ asset('style/img/blog/blog-post-2.jpg') }}" class="img-fluid" alt="Blog image" />
                    <div class="blog-excerpt open-sans-font pb-5">
                        <p>Dekat dengan kating alias kakak tingkat gampang gampang susah lho. Namun, semakin banyak
                            dekat dengan kakak tingkat, maka kamu akan memiliki banyak pengetahuan. Memasuki dunia yang baru
                            kadang sangat mengasikkan dan menegangkan, apalagi kalau kita mempunyai kenalan yang bisa
                            menjaga dan diandalkan lebih senior dari kita wah pasti lebih seru lagi ya guys.
                            Ingin belajar hal baru dan bersosialisasi dengan lingkungan baru juga bisa jadi motivasi buat
                            aktif. Dengan berperan aktif, kita bisa kenal sama orang-orang baru, suasana baru, dan
                            pandangan-pandangan baru juga. Waktu kita udah mutusin untuk terlibat dalam satu kegiatan
                            organisasi, kita bakal dituntut untuk bekerja bareng berbagai macam orang, termasuk kakak kelas.
                            salah satu cara buat bisa menjalin pertemenan dengan kakak kelas, mungkin cara/ tips ini bisa
                            membantu kalian nih buat wujudkannya.</p>
                        <p>Pastikan kita selalu tersenyum ramah waktu berpapasan sama kakak kelas dan jangan pernah ragu
                            buat menyapa mereka lebih dulu. Dengan tersenyum, orang lain bakal menganggap kita sebagai orang
                            yang friendly dan ramah, dan mereka akan tertarik buat berhubungan lebih jauh sama kita. Nggak
                            usah takut dianggap SKSD, karena terkadang bisa jadi sebenernya para senior itu pengen nyapa
                            kita juga, tapi mereka cuma mau menunggu reaksi kita lebih dulu. Bersikap ramah itu pahala lo
                            guys, nggak ada jeleknya kan, hehehe. </p>
                        <p>Kadang untuk memulai pembicaraan denga senior dalam suatu suasana bareng tertentu butuh modal
                            keberanian ya guys, bahkan kita super duper nggak percaya diri deh kalau senior kita lebih
                            segalanya dari kita atau bahkan kita nggak tau mau ngomong apa.
                            Hindari semua pikiran negatif di kepala kita dan beranilah untuk ngajak senior itu ngobrol.
                            Tanpa kita sadari, nantinya obrolan itu pasti bakal berlanjut dengan sendirinya. Satu obrolan
                            pertama aja bisa membuka jalan baru buat kita berhubungan lebih lanjut sama senior-senior kita.
                            Di lain waktu, saat kita nggak sengaja ketemu lagi sama senior itu, dia pasti akan ingat kita
                            dan ngajak kita untuk ngobrol bareng lagi.</p>
                    </div>
                    <!-- Article Content Ends -->
                </article>
                <!-- Article Ends -->
            </div>
        </div>
    </section>
@endsection
