@extends('layout')

@section('judul')
    My Blog
@endsection

@section('konten')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>blog</span></h1>
        <span class="title-bg">posts</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="blog-post6">
        <div class="container">
            <div class="row">
                <!-- Article Starts -->
                <article class="col-12">
                    <!-- Meta Starts -->
                    <div class="meta open-sans-font">
                        <span><i class="fa fa-user"></i> melsi</span>
                        <span class="date"><i class="fa fa-calendar"></i> 26 September 2021</span>
                        <span><i class="fa fa-tags"></i>melsi.com</span>
                    </div>
                    <!-- Meta Ends -->
                    <!-- Article Content Starts -->
                    <h1 class="text-uppercase text-capitalize">Bagaimana Cara Mengikuti Organisasi</h1>
                    <img src="{{ asset('style/img/blog/blog-post-6.jpg') }}" class="img-fluid" alt="Blog image" />
                    <div class="blog-excerpt open-sans-font pb-5">
                        <p>Ikutilah organisasi yang bisa menunjang minat dan bakatmu. Jangan ikuti organisasi
                            abal-abal ya. Dunia perkuliahan tidak hanya belajar. Mungkin kata-kata ini sering kita dengar
                            dari kalangan mahasiswa, baik dari teman kita maupun dari kakak tingkat yang lebih
                            berpengalaman. Yap, berkuliah bukan berarti kita hanya sebatas belajar di dalam kelas saja. Kita
                            dituntut juga untuk menguasai beberapa soft skills umum yang nantinya akan bermanfaat dalam
                            dunia kerja. Sebut saja diantaranya yaitu teamwork, komunikasi, koordinasi, kemampuan
                            presentasi, dan lain sebagainya. Mengikuti organisasi adalah salah satu cara untuk mengembangkan
                            soft skills kita, terutama di bidang manajemen acara. Namun ternyata, tidak sedikit dari
                            mahasiswa yang mengikuti organisasi sebatas menjadi “pion organisasi”. Mereka lebih seperti
                            robot yang menjalankan tugas, yaitu proker-proker yang ada.</p>
                        <p>Organisasi manapun pasti akan mengadakan internalisasi, terutama di awal kepengurusan. Baik
                            internalisasi divisi/departemen maupun secara komunal, tujuannya adalah supaya
                            anggota-anggotanya lebih mengenal satu sama lain.
                            Tergantung dari jenis organisasinya, internalisasi diadakan dalam interval waktu tertentu.
                            Sebagai anggota organisasi, sangat baik bagi kita untuk tidak absen di setiap internalisasi,
                            utamanya ketika internalisasi diadakan untuk semua anggota. Tidak banyak momen yang dapat
                            menyatukan seluruh anggota organisasi bersama, sehingga kita lebih baik memanfaatkannya sebaik
                            mungkin. Apalagi kebanyakan dari kita punya tujuan ikut organisasi untuk menambah relasi, kan?
                            Dengan mengikuti internalisasi, kita bisa bertemu lebih banyak orang dan mengenal lebih banyak
                            rekan kerja. Sejalan dengan hal itu, kita juga akan belajar bagaimana kita berkomunikasi dengan
                            bermacam-macam orang dengan berbagai sifat. </p>
                        <p>Ketika kita bergabung dalam suatu organisasi, kegiatan yang tepat untuk mengisi waktu luang kita
                            adalah menghabiskan waktu di sekretariat. Baik kita mau mengerjakan tugas, sekedar membaca buku,
                            atau numpang wifi, sekretariat adalah tempat yang cocok untuk kegiatan-kegiatan tersebut.
                            Anggaplah sekretariat seperti rumah kedua atau ketiga, karena disinilah kita akan banyak
                            berkutat dengan rapat-rapat maupun kumpul biasa.
                            Nongkrong di sekretariat juga memberi kita kesempatan untuk bisa mengobrol lebih dalam dengan
                            teman-teman maupun atasan-atasan organisasi kita. Dari mereka, kita bisa mendapat banyak
                            pengalaman serta cerita baru.</p>
                    </div>
                    <!-- Article Content Ends -->
                </article>
                <!-- Article Ends -->
            </div>
        </div>
    </section>
@endsection
