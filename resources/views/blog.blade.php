@extends('layout')

@section('judul')
    My Blog
@endsection

@section('konten')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>blog</span></h1>
        <span class="title-bg">achievement</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="blog">
        <div class="container">
            <!-- Articles Starts -->
            <div class="row">
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post1" class="d-block position-relative overflow-hidden">
                                <img src="{{ asset('style/img/blog/blog-post-1.jpg') }}" class="img-fluid"
                                    alt="Blog Post">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post1">Cara Ampuh Menang Lomba PKM</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>“Ide yang segar merupakan ide yang baru, menarik, kreatif, unik dan kalau bisa merupakan
                                    inovasi yang lebih baik dari ide yang mirip sebelumnya. Yuk Cari Tahu lebih Jauh Cara
                                    Ampuh Menang Lomba PKM”.
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post2" class="d-block position-relative overflow-hidden">
                                <img src="{{ asset('style/img/blog/blog-post-2.jpg') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post2">Tips Bersosialisasi dengan Kakak Tingkat</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>"Dekat dengan kating alias kakak tingkat gampang gampang susah lho. Namun, semakin banyak
                                    dekat dengan kakak tingkat, maka kamu akan memiliki banyak pengetahuan. Yuk Cari Tahu
                                    Bagaimana Tipsnya!".
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post3" class="d-block position-relative overflow-hidden">
                                <img src="{{ asset('style/img/blog/blog-post-3.jpg') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post3">Cara Jitu Memiliki Banyak Teman</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>"Ikuti banyak kegiatan di kampus, seperti organisasi, maka kamu akan memiliki banyak
                                    teman. Cari teman yang pas di hati ya. Yuk Cari Tahu Bagaimana Cara Memiliki Banyak
                                    Teman Agar
                                    Circlemu Bertambah!".
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post4" class="d-block position-relative overflow-hidden">
                                <img src="{{ asset('style/img/blog/blog-post-4.jpg') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post4">Cara Ampuh Menang PMW</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>"Komponen utama dalam PMW adalah ide yang baru, menarik, kreatif, unik dan memiliki
                                    manfaat dimasyarakat jika diapllikasikan. Yuk Cari Tahu lebih Jauh Cara
                                    Ampuh Menang PMW”.
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post5" class="d-block position-relative overflow-hidden">
                                <img src="{{ asset('style/img/blog/blog-post-5.jpg') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post5">Langkah-langkah Lolos Didanai PHP2D</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>"Trik lolos didanai PHP2D sendiri adalah ide yang dipaparkan memiliki nilai dan manfaat
                                    di masyarakat. Yuk Cari Tahu lebih Jauh Lagi”.
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <article class="post-container">
                        <div class="post-thumb">
                            <a href="/blog-post6" class="d-block position-relative overflow-hidden">
                                <img src="{{ asset('style/img/blog/blog-post-6.jpg') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="post-content">
                            <div class="entry-header">
                                <h3><a href="/blog-post6">Bagaimana Cara Mengikuti Organisasi</a></h3>
                            </div>
                            <div class="entry-content open-sans-font">
                                <p>"Ikutilah organisasi yang bisa menunjang minat dan bakatmu. Jangan ikuti organisasi
                                    abal-abal ya. Yuk Cari Tahu lebih jauh lagi!".
                                </p>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Article Ends -->
                <!-- Pagination Starts -->
                <div class="col-12 mt-4">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center mb-0">
                            <li class="page-item"><a class="page-link" href="/blog">1</a></li>
                            <li class="page-item active"><a class="page-link" href="/blog">2</a></li>
                            <li class="page-item"><a class="page-link" href="/blog">3</a></li>
                            <li class="page-item"><a class="page-link" href="/blog">4</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- Pagination Ends -->
            </div>
            <!-- Articles Ends -->
        </div>

    </section>

@endsection
